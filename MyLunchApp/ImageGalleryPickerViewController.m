//
//  ImageGalleryPickerViewController.m
//  MyLunchApp
//
//  Created by Muhammad Azeem on 25/02/2015.
//  Copyright (c) 2015 Muhammad Azeem. All rights reserved.
//

#import "ImageGalleryPickerViewController.h"
#import "AppDelegate.h"
#import "UIViewController+Alert.h"
#import "LunchDetailViewController.h"

static NSString* imageCellIdentifier = @"ImageCellIdentifier";
static NSString* buttonCellIdentifier = @"ButtonCellIdentifier";

@implementation ImageGalleryPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    images = [NSMutableArray arrayWithCapacity:MAX_IMAGES];
    
    UIActivityIndicatorView *activatiorIndicator = [[UIActivityIndicatorView alloc] initWithFrame:self.view.bounds];
    UIView *blackView = [[UIView alloc] initWithFrame:self.view.bounds];
    blackView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.6];
    activatiorIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [self.view addSubview:blackView];
    [self.view addSubview:activatiorIndicator];
    [activatiorIndicator startAnimating];
    
    dispatch_async(dispatch_queue_create("ImageConversion", NULL), ^{
        PFFile *photo1 = self.lunch[@"Photo1"];
        
        NSData *imageData1 = [photo1 getData];
        if (nil != imageData1) {
            [images addObject:[UIImage imageWithData:imageData1]];
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [activatiorIndicator stopAnimating];
            [activatiorIndicator removeFromSuperview];
            [blackView removeFromSuperview];
            
            [self.collectionView reloadData];
        });
    });
}

- (void)addImage {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate = self;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.destinationViewController isKindOfClass:[LunchDetailViewController class]]) {
        LunchDetailViewController *vc = (LunchDetailViewController *)segue.destinationViewController;
        
        vc.lunch = self.lunch;
    }
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return MIN(images.count + 1, MAX_IMAGES);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    long itemNum = (indexPath.section + 1) * (indexPath.row + 1);
    
    UICollectionViewCell *collectionViewCell;
    // Show image cell
    if (itemNum <= MIN(images.count, MAX_IMAGES)) {
        
        ImageCell *imageCell = (ImageCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:imageCellIdentifier forIndexPath:indexPath];
        
        imageCell.image.image = images[itemNum - 1];
        
        collectionViewCell = imageCell;
        
    } else {
        
        ButtonCell *buttonCell = (ButtonCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:buttonCellIdentifier forIndexPath:indexPath];
        
        buttonCell.delegate = self;
        
        collectionViewCell = buttonCell;
        
    }
    
    return collectionViewCell;
}

#pragma mark - ButtonCellDelegate Methods

- (void)buttonPressed {
    [self addImage];
}

#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    if (nil == image)
        return;
    
    // Scale the image
    UIImage *scaledImage = [UIImage imageWithCGImage:image.CGImage scale:0.6 orientation:image.imageOrientation];
    
    [images addObject:scaledImage];
    [self.collectionView reloadData];
}

#pragma mark - Actions
- (IBAction)continueButtonPressed:(id)sender {
    UIActivityIndicatorView *activatiorIndicator = [[UIActivityIndicatorView alloc] initWithFrame:self.view.bounds];
    UIView *blackView = [[UIView alloc] initWithFrame:self.view.bounds];
    blackView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.6];
    activatiorIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [self.view addSubview:blackView];
    [self.view addSubview:activatiorIndicator];
    [activatiorIndicator startAnimating];
    
    dispatch_async(dispatch_queue_create("ImageAssignment", NULL), ^{
        for (int i = 0; i < MIN(MAX_IMAGES, images.count); i++) {
            NSData *imageData = UIImageJPEGRepresentation(images[i], 0.5);
            PFFile *photo = [PFFile fileWithData:imageData];
            
            NSString *key = [NSString stringWithFormat:@"Photo%d", i + 1];
            self.lunch[key] = photo;
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [activatiorIndicator stopAnimating];
            [activatiorIndicator removeFromSuperview];
            [blackView removeFromSuperview];
            
            [self performSegueWithIdentifier:@"LunchDetailSegue" sender:self];
        });
    });
}

@end
