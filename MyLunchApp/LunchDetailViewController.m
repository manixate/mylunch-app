//
//  LunchDetailViewController.m
//  MyLunchApp
//
//  Created by Muhammad Azeem on 25/02/2015.
//  Copyright (c) 2015 Muhammad Azeem. All rights reserved.
//

#import "LunchDetailViewController.h"
#import "LunchListViewController.h"
#import "ImageCell.h"

static NSString* imageCellIdentifier = @"ImageCellIdentifier";

@implementation LunchDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lunchTitle.text = self.lunch[@"Name"];
    self.lunchDescription.text = self.lunch[@"Description"];
    
    images = [NSMutableArray array];
    
    UIActivityIndicatorView *activatiorIndicator = [[UIActivityIndicatorView alloc] initWithFrame:self.view.bounds];
    UIView *blackView = [[UIView alloc] initWithFrame:self.view.bounds];
    blackView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.4];
    activatiorIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [self.view addSubview:blackView];
    [self.view addSubview:activatiorIndicator];
    [activatiorIndicator startAnimating];
    
    dispatch_async(dispatch_queue_create("ImageConversion", NULL), ^{
        PFFile *photo1 = self.lunch[@"Photo1"];
        PFFile *photo2 = self.lunch[@"Photo2"];
        PFFile *photo3 = self.lunch[@"Photo3"];
        PFFile *photo4 = self.lunch[@"Photo4"];
        
        UIImage *imageData1 = [UIImage imageWithData:[photo1 getData]];
        UIImage *imageData2 = [UIImage imageWithData:[photo2 getData]];
        UIImage *imageData3 = [UIImage imageWithData:[photo3 getData]];
        UIImage *imageData4 = [UIImage imageWithData:[photo4 getData]];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (nil != imageData1)
                [images addObject: imageData1];
            
            if (nil != imageData2)
                [images addObject: imageData2];
            
            if (nil != imageData3)
                [images addObject: imageData3];
            
            if (nil != imageData4)
                [images addObject: imageData4];
            
            [activatiorIndicator stopAnimating];
            [activatiorIndicator removeFromSuperview];
            [blackView removeFromSuperview];
            
            [self.lunchImagesCollectionView reloadData];
        });
    });
}

#pragma mark - UICollectionViewControllerDataSource Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return images.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    long itemNum = (indexPath.section + 1) * (indexPath.row + 1);
    
    ImageCell *imageCell = (ImageCell *)[self.lunchImagesCollectionView dequeueReusableCellWithReuseIdentifier:imageCellIdentifier forIndexPath:indexPath];
    
    imageCell.image.image = images[itemNum - 1];
    
    return imageCell;
}

- (IBAction)doneButtonPressed:(id)sender {
    if (self.dontSave) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        return;
    }
    
    UIActivityIndicatorView *activatiorIndicator = [[UIActivityIndicatorView alloc] initWithFrame:self.view.bounds];
    UIView *blackView = [[UIView alloc] initWithFrame:self.view.bounds];
    blackView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.6];
    activatiorIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [self.view addSubview:blackView];
    [self.view addSubview:activatiorIndicator];
    [activatiorIndicator startAnimating];
    
    dispatch_async(dispatch_queue_create("SaveData", NULL), ^{
        [self.lunch save];
        
        // Create new lunch from now
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AddNewLunch" object:nil];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [activatiorIndicator stopAnimating];
            [activatiorIndicator removeFromSuperview];
            [blackView removeFromSuperview];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        });
    });
}
@end
