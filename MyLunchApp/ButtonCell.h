//
//  ButtonCell.h
//  MyLunchApp
//
//  Created by Muhammad Azeem on 25/02/2015.
//  Copyright (c) 2015 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ButtonCellDelegate <NSObject>

- (void)buttonPressed;

@end

@interface ButtonCell : UICollectionViewCell

@property (nonatomic, weak) id<ButtonCellDelegate> delegate;

- (IBAction)addButtonPressed:(id)sender;

@end
