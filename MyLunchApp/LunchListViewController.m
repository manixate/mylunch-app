//
//  LunchListViewController.m
//  MyLunchApp
//
//  Created by Muhammad Azeem on 26/02/2015.
//  Copyright (c) 2015 Muhammad Azeem. All rights reserved.
//

#import "LunchListViewController.h"
#import "LunchDetailViewController.h"

@implementation LunchListViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (nil != self) {
        self.parseClassName = @"Lunch";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"Title";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = YES;
        
        // The number of objects to show per page
        self.objectsPerPage = 10;
    }
    
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

- (PFQuery *)queryForTable {
    PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
    
    return query;
}

#pragma mark - PFQueryTableViewDataSource Methods
- (PFTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    static NSString *simpleTableIdentifier = @"CellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    // Configure the cell
    PFFile *thumbnail = [object objectForKey:@"Photo1"];
    PFImageView *thumbnailImageView = (PFImageView*)[cell viewWithTag:100];
    thumbnailImageView.image = [UIImage imageNamed:@"placeholder.jpg"];
    thumbnailImageView.file = thumbnail;
    [thumbnailImageView loadInBackground];
    
    UILabel *nameLabel = (UILabel*) [cell viewWithTag:101];
    nameLabel.text = [object objectForKey:@"Name"];
    
    UILabel *descriptionLabel = (UILabel*) [cell viewWithTag:102];
    descriptionLabel.text = [object objectForKey:@"Description"];
    
    return (PFTableViewCell *)cell;

}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    PFObject *lunch = [self.objects objectAtIndex:indexPath.row];
    
    if ([segue.destinationViewController isKindOfClass:[LunchDetailViewController class]]) {
        LunchDetailViewController *vc = (LunchDetailViewController *)segue.destinationViewController;
        
        vc.lunch = lunch;
        vc.dontSave = true;
    }
}

@end
