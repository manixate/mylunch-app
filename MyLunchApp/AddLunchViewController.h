//
//  AddLunchInfoViewController.h
//  MyLunchApp
//
//  Created by Muhammad Azeem on 26/02/2015.
//  Copyright (c) 2015 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface AddLunchViewController : UIViewController {
    PFObject *lunch;
    int selectedImageCount;
}

@property (nonatomic, strong) NSData* imageData;

@property (weak, nonatomic) IBOutlet UITextField *lunchName;
@property (weak, nonatomic) IBOutlet UITextView *lunchDescription;

@end
