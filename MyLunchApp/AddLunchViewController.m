//
//  AddLunchInfoViewController.m
//  MyLunchApp
//
//  Created by Muhammad Azeem on 26/02/2015.
//  Copyright (c) 2015 Muhammad Azeem. All rights reserved.
//

#import "AddLunchViewController.h"
#import "AppDelegate.h"
#import "UIViewController+Alert.h"
#import "LunchDetailViewController.h"
#import "ImageGalleryPickerViewController.h"
#import <Parse/Parse.h>

@implementation AddLunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Create parse object
    lunch = [PFObject objectWithClassName:@"Lunch"];
    
    if (nil != self.imageData) {
        PFFile *photo1 = [PFFile fileWithData:self.imageData];
        
        lunch[@"Photo1"] = photo1;
    }
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"AddNewLunch" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        lunch = [PFObject objectWithClassName:@"Lunch"];
        
        self.lunchName.text = lunch[@"Name"];
        self.lunchDescription.text = lunch[@"Description"];
    }];
    
    self.lunchName.text = lunch[@"Name"];
    self.lunchDescription.text = lunch[@"Description"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if (_lunchName.text.length == 0) {
        [self showAlert:@"Information" message:@"Please enter your lunch name."];
        return false;
    }
    
    if (_lunchDescription.text.length == 0) {
        [self showAlert:@"Information" message:@"Please enter your lunch description."];
        return false;
    }
    
    return true;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    lunch[@"Name"] = self.lunchName.text;
    lunch[@"Description"] = self.lunchDescription.text;
    
    if ([segue.destinationViewController isKindOfClass:[LunchDetailViewController class]]) {
        LunchDetailViewController *vc = (LunchDetailViewController *)segue.destinationViewController;
        
        vc.lunch = lunch;
    } else if ([segue.destinationViewController isKindOfClass:[ImageGalleryPickerViewController class]]) {
        ImageGalleryPickerViewController *vc = (ImageGalleryPickerViewController *)segue.destinationViewController;
        
        vc.lunch = lunch;
    }
}

@end
