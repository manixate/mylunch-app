//
//  LunchDetailViewController.h
//  MyLunchApp
//
//  Created by Muhammad Azeem on 25/02/2015.
//  Copyright (c) 2015 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface LunchDetailViewController : UIViewController {
    NSMutableArray *images;
}

@property (weak, nonatomic) IBOutlet UILabel *lunchTitle;
@property (weak, nonatomic) IBOutlet UITextView *lunchDescription;

@property (weak, nonatomic) IBOutlet UICollectionView *lunchImagesCollectionView;

@property (nonatomic) BOOL dontSave;

@property (nonatomic, strong) PFObject *lunch;

- (IBAction)doneButtonPressed:(id)sender;
@end
