//
//  ButtonCell.m
//  MyLunchApp
//
//  Created by Muhammad Azeem on 25/02/2015.
//  Copyright (c) 2015 Muhammad Azeem. All rights reserved.
//

#import "ButtonCell.h"

@implementation ButtonCell

- (IBAction)addButtonPressed:(id)sender {
    if (self.delegate) {
        [self.delegate buttonPressed];
    }
}
@end
