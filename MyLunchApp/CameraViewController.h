//
//  CameraViewController.h
//  MyLunchApp
//
//  Created by Muhammad Azeem on 26/02/2015.
//  Copyright (c) 2015 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface CameraViewController : UIViewController {
    NSData *captureImageData;
}

@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureStillImageOutput *stillImageOutput;

- (IBAction)captureImageButtonPressed:(id)sender;
@end
