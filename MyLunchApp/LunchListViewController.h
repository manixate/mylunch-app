//
//  LunchListViewController.h
//  MyLunchApp
//
//  Created by Muhammad Azeem on 26/02/2015.
//  Copyright (c) 2015 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <ParseUI/ParseUI.h>

@interface LunchListViewController : PFQueryTableViewController {
}

@end
