//
//  main.m
//  MyLunchApp
//
//  Created by Muhammad Azeem on 25/02/2015.
//  Copyright (c) 2015 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
