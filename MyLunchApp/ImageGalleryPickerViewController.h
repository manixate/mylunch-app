//
//  ImageGalleryPickerViewController.h
//  MyLunchApp
//
//  Created by Muhammad Azeem on 25/02/2015.
//  Copyright (c) 2015 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ButtonCell.h"
#import "ImageCell.h"
#import <Parse/Parse.h>

static const int MAX_IMAGES = 4;

@interface ImageGalleryPickerViewController : UICollectionViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, ButtonCellDelegate> {
    UIImagePickerController* _imagePicker;
    
    NSMutableArray *images;
}

@property (nonatomic, strong) PFObject *lunch;

- (IBAction)continueButtonPressed:(id)sender;
@end
