//
//  ImageCell.h
//  MyLunchApp
//
//  Created by Muhammad Azeem on 25/02/2015.
//  Copyright (c) 2015 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end
